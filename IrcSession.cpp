#include "IrcSession.h"
#include <iostream>
#include <QStringList>

using namespace std;

namespace {
  static int sessionId = 0;
  int generateSessionId (){
    // May not be thread safe?
    return sessionId++;
  }

  void event_connect(irc_session_t * session, const char * event, const char * origin, const char ** params, unsigned int count){
    IrcSession *ctx = static_cast<IrcSession*>(irc_get_ctx(session));
    QStringList paramList;
    for (unsigned int i = 0; i < count; i++)
      paramList.push_back(params[i]);
    ctx->event_connect(event, origin, paramList);
  }
  void event_notice(irc_session_t * session, const char * event, const char * origin, const char ** params, unsigned int count){
    IrcSession *ctx = static_cast<IrcSession*>(irc_get_ctx(session));
    QStringList paramList;
    for (unsigned int i = 0; i < count; i++)
      paramList.push_back(params[i]);
    ctx->event_notice(event, origin, paramList);
  }

  void event_channel(irc_session_t * session, const char * event, const char * origin, const char ** params, unsigned int count){
    IrcSession *ctx = static_cast<IrcSession*>(irc_get_ctx(session));
    QStringList paramList;
    for (unsigned int i = 0; i < count; i++)
      paramList.push_back(params[i]);
    ctx->event_channel(event, origin, paramList);
  }
  void event_numeric(irc_session_t * session, unsigned int event, const char * origin, const char ** params, unsigned int count){
    IrcSession *ctx = static_cast<IrcSession*>(irc_get_ctx(session));
    QStringList paramList;
    for (unsigned int i = 0; i < count; i++)
      paramList.push_back(params[i]);
    ctx->event_numeric(event, origin, paramList);
  }
  void event_join(irc_session_t * session, const char * event, const char * origin, const char ** params, unsigned int count){
    IrcSession *ctx = static_cast<IrcSession*>(irc_get_ctx(session));
    QStringList paramList;
    for (unsigned int i = 0; i < count; i++)
      paramList.push_back(params[i]);
    ctx->event_join(event, origin, paramList);
  }
  void event_part(irc_session_t * session, const char * event, const char * origin, const char ** params, unsigned int count){
    IrcSession *ctx = static_cast<IrcSession*>(irc_get_ctx(session));
    QStringList paramList;
    for (unsigned int i = 0; i < count; i++)
      paramList.push_back(params[i]);
    ctx->event_part(event, origin, paramList);
  }
  void event_topic(irc_session_t * session, const char * event, const char * origin, const char ** params, unsigned int count){
    IrcSession *ctx = static_cast<IrcSession*>(irc_get_ctx(session));
    QStringList paramList;
    for (unsigned int i = 0; i < count; i++)
      paramList.push_back(params[i]);
    ctx->event_topic(event, origin, paramList);
  }

}


IrcSession::IrcSession():
  m_sessionId(generateSessionId()),
  callbacks(new irc_callbacks_t),
  m_session(0),
  connection(this)
{
  memset(callbacks.get(),0,sizeof(irc_callbacks_t));
  cout << "creating irc session" << endl;
  callbacks->event_connect = ::event_connect;
  callbacks->event_notice = ::event_notice;
  callbacks->event_channel = ::event_channel;
  callbacks->event_join = ::event_join;
  callbacks->event_part = ::event_part;
  callbacks->event_topic = ::event_topic;
  callbacks->event_numeric = ::event_numeric;

  m_session.reset(irc_create_session(callbacks.get()), irc_destroy_session);
  irc_set_ctx(m_session.get(),this);
}

IrcSession::~IrcSession(){
  irc_disconnect(m_session.get());
  connection.wait();
}

IrcSession::IrcSessionCode IrcSession::connect(const QString &address, int port, const QString &nick, const QString &username, const QString &password, const QString &realname)
{
  m_nickname=nick;
  if (irc_connect(m_session.get(), 
                  address.toUtf8().data(), 
                  port, 
                  password.toUtf8().data(), 
                  nick.toUtf8().data(), 
                  username.toUtf8().data(),
                  realname.toUtf8().data()
                  )){
    cout << "Irc connect failed" << endl;
    return CONNECT_FAILED;
  } else {
    cout << "Irc connect succeeded" << endl;
    connection.start();
    return CONNECT_OK;
  }
  
}

bool IrcSession::isConnected() const
{
  return irc_is_connected(m_session.get());
}

void IrcSession::joinChannel(const QString &channel, const QString &password)
{
  irc_cmd_join(m_session.get(), channel.toUtf8().data(), password.toUtf8().data());
}
void IrcSession::partChannel(const QString &channel)
{
  irc_cmd_part(m_session.get(), channel.toUtf8().data());
}
void IrcSession::sendMessage(const QString &dest, const QString &message)
{
  irc_cmd_msg(m_session.get(), dest.toUtf8().data(), message.toUtf8().data());
}

void IrcSession::changeNick(const QString &nick)
{
}

void IrcSession::event_connect(const QString &event, const QString &origin, const QStringList &params){
  joinChannel("#random_channel_test");
}
void IrcSession::event_notice(const QString &event, const QString &origin, const QStringList &params){
  emit receivedNotice(origin, params[1]);
}
void IrcSession::event_channel(const QString &event, const QString &origin, const QStringList &params){
  emit receivedChannelMessage(m_sessionId, params[0], origin.split("!")[0], params[1]);
}
void IrcSession::event_numeric(unsigned int event, const QString &origin, const QStringList &params){
  switch (event){
    case LIBIRC_RFC_RPL_TOPIC:
      emit topicChanged(m_sessionId, params[1], params[2]);
      cout << "topic event" << endl;
      break;
    case LIBIRC_RFC_RPL_NOTOPIC:
      cout << "no topic event" << endl;
      break;
    case LIBIRC_RFC_RPL_NAMREPLY:
    {
      QStringList ops;
      QStringList voices;
      QStringList normal;
      QStringList names = QString(params[3]).split(' ');

      QMap<QString,QString> sortmap;
      foreach (QString s, names){
        sortmap[s.toLower()] = s;
        if (s[0] == '@')
          ops.push_back(s.toLower());
        else if (s[0] == '+')
          voices.push_back(s.toLower());
        else 
          normal.push_back(s.toLower());
      }

      ops.sort(); voices.sort(); normal.sort();
      QStringList sorted_lower_names = ops + voices + normal;
      QStringList sorted_names;
      foreach (QString s, sorted_lower_names){
        sorted_names.push_back(sortmap[s]);
        cout << sortmap[s].toUtf8().data() << endl;
      }
      emit namesChanged(m_sessionId, params[2], sorted_names);
      break;
    }
    default:
      break;
  }
}
void IrcSession::event_join(const QString &event, const QString &origin, const QStringList &params){
  QString nickname = origin.split("!")[0];
  if (m_nickname == nickname){
    emit joinedChannel(params[0]);
    irc_cmd_topic(m_session.get(), params[0].toUtf8().data(), NULL);
  }
  irc_cmd_names(m_session.get(), params[0].toUtf8().data());
}
void IrcSession::event_part(const QString &event, const QString &origin, const QStringList &params){
  QString nickname = origin.split("!")[0];
  irc_cmd_names(m_session.get(), params[0].toUtf8().data());
}
void IrcSession::event_topic(const QString &event, const QString &origin, const QStringList &params){
  cout << "topic changed" << endl;
  emit topicChanged(m_sessionId, params[0], params[1]);
}
