#include "CommandParser.h"
#include <QMap>

namespace {
  QMap<QString, CommandParser::UserCommand::CommandType> command_map;

  bool init_map (){
    command_map["kick"] = CommandParser::UserCommand::KICK;
    command_map["join"] = CommandParser::UserCommand::JOIN;
    command_map["part"] = CommandParser::UserCommand::PART;
    command_map["server"] = CommandParser::UserCommand::SERVER;
    return true;
  }
}

CommandParser::CommandParser() {
}

CommandParser::UserCommand CommandParser::parseCommand(const QString &command_string){
  UserCommand retval;
  static bool map_init = init_map();
  const int buffer_length = command_string.length()+1;

  std::unique_ptr<char[]> command_cstr(new char[buffer_length]);
  memcpy(command_cstr.get(), command_string.toUtf8().data(), buffer_length);
  command_cstr[buffer_length-1] = '\0';
  
  bool in_string = false;
  bool in_command = false;
  char *start_pos = 0;

  for (int i = 0; i < buffer_length; ++i){
    switch (command_cstr[i]){
      case '/':
        in_command = true;
        start_pos = command_cstr.get() + 1;
        break;
      case '"':
        in_string = !in_string;
        break;
      case '\0':
      case ' ':
        if (in_command){
          command_cstr[i] = '\0';
          QString command = QString(start_pos).toLower();
          start_pos = command_cstr.get() + i + 1;

          if (command_map.contains(command)){
            retval.type = command_map[command];
          }

          in_command = false;
        } 
        else if (!in_string){
          command_cstr[i] = '\0';
          retval.params->push_back(QString(start_pos));                        
          start_pos = command_cstr.get() + i + 1;
        }
        break;
      default:
        break;
    }
  }
  return retval;
}
