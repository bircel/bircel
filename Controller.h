#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QObject>
#include <QString>
#include <QMainWindow>
#include <QMdiSubWindow>
#include <QVariant>
#include <QCloseEvent>
#include <map>
#include "IrcSession.h"
#include "CommandParser.h"

class Controller : public QObject {
  Q_OBJECT

  public:
    Controller();

    void startProgram();

  public slots:
    void updateSessionText(const QString &origin, const QString &message);
    void updateChannelChat(int sessionId, const QString &channel, const QString &nickname, const QString &message);
    void updateChannelTopic(int sessionId, const QString &channel, const QString &message);
    void updateChannelNames(int sessionId, const QString &channel, const QStringList &names);

    void createChannel(const QString &channel);
    void closeChannel(const int sessionId, const QString &channel);

    void handleSessionInput();
    void handleChannelInput();

    void createSession(const QString& hostname, int port, const QString& nick, const QString& username="", const QString& password="", const QString& realname="");
    void createSessionWindow();

  private:
    void createSessionWindow(const IrcSession *session);
    void handleUserCommand(int sessionId, const CommandParser::UserCommand& userCommand, const QString& channel="");

    std::shared_ptr<QMainWindow> m_mainWindow;
    std::map<int,std::shared_ptr<IrcSession> > m_sessions;
    QString m_default_nick;
};

class ChannelSubWindow : public QMdiSubWindow
{
  Q_OBJECT
  public:
    ChannelSubWindow(QWidget *p):QMdiSubWindow(p){}

  signals:
    void closingChannel(const int sessionId, const QString channelName);
  protected:
    void closeEvent(QCloseEvent * event){
      emit closingChannel(property("SessionId").toInt(), property("ChannelName").toString());
      event->accept();
    }
};

#endif
