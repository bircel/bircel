#ifndef COMMAND_PARSER_H_
#define COMMAND_PARSER_H_

#include <memory>
#include <QVector>
#include <QString>

class CommandParser
{
  public:
    CommandParser();
    struct UserCommand {
      enum CommandType {
        UNKNOWN,
        JOIN,
        PART,
        KICK,
        SERVER          
      };

      UserCommand() : type(UNKNOWN), params(new QVector<QString>()){}

      CommandType type;
      std::shared_ptr<QVector<QString> > params;

    };

    static UserCommand parseCommand(const QString &command);

  private:
};

#endif

