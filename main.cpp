#include <QApplication>
#include <QTextCodec>
#include "Controller.h"

int main(int argc, char** argv)
{
  QApplication app (argc, argv);
  QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));

  Controller c;
  c.startProgram();

  return app.exec();
}
